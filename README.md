= M4 HTML

== Passos per configurar el GIT

1. Crear usuari a Gitlab.
2. Crear projecte M4 HTML.
3. Crear usuari a Linux.
4. Configurar GIT:
+
[source]
----
git --config global user.email maria@gmail.com
git --config global user.name Maria López
----

5. Crear clau criptogràfica:
+
[source]
----
ssh-keygen -t rsa -C "maria@gmail.com" -b 4096
----

6. Pujar clau criptogràfica al GitLab:
+
- Perfil de l'usuari.
- Claus SSH
- Enganxar clau pública (`.ssh/id_rsa.pub`)

7. Flux de treball bàsic en línia de comandes:
+
1. El primer cop: `git clone git@gitlab.com:joanq/m4-html.git`
2. Per cada grup de canvis:
+
[source]
----
1. git add .     Marca els fitxers per guardar-los
2. git commit -m "missatge"    Guarda els fitxers al git local
3. git push      Puja els canvis al GitLab
----
+
